const express = require('express');

const RoleController = require('./controllers/RoleController');
const UserController = require('./controllers/UserController');
const LoginController = require('./controllers/LoginController');
const MedicalRecordController = require('./medical_record/controllers/MedicalRecordController');
const ExamController = require('./medical_record/controllers/ExamController');
const DocumentController = require('./medical_record/controllers/DocumentController');
const ExamRequestController = require('./medical_record/controllers/ExamRequestController');
const RecordController = require('./medical_record/controllers/RecordController');
const CertificateController = require('./medical_record/controllers/CertificateController');
const HomeController = require('./controllers/HomeController');

const routes = express.Router();
const AuthMiddleware = require('./middlewares/auth');

routes.get('/roles', RoleController.index);
routes.post('/roles', RoleController.store);

routes.get('/users/:role_id/role', UserController.index);
routes.post('/users/:role_id/role', UserController.store);
routes.get('/users/list', AuthMiddleware, UserController.list);

routes.delete('/admin/del-user', AuthMiddleware, UserController.delete);

routes.post('/doctor/create-medical-record', MedicalRecordController.store);
routes.post('/doctor/:mr_id/create-exam', ExamController.store);
routes.post('/doctor/:mr_id/create-document', DocumentController.store);
routes.post('/doctor/:mr_id/create-exam-request', ExamRequestController.store);
routes.post('/doctor/:mr_id/create-record', RecordController.store);
routes.post('/doctor/:mr_id/create-certificate', CertificateController.store);

routes.get('/doctor/list-medical-records', MedicalRecordController.index);
routes.get('/doctor/list-exams', ExamController.list);
routes.get('/doctor/list-documents', DocumentController.list);
routes.get('/doctor/list-exam-requests', ExamRequestController.list);
routes.get('/doctor/list-records', RecordController.list);
routes.get('/doctor/list-certificates', CertificateController.list);

routes.post('/login', LoginController.auth);

routes.get('/home', HomeController.index);
routes.post('/home', HomeController.store);

module.exports = routes;

