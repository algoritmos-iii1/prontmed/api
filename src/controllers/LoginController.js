const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const configAuth = require('../config/auth');
//const session = require('express-session');

module.exports = {

  async auth(req, res) {

    const email = req.body.email;
    const password = req.body.password;

    const user = User.findOne({ where: { email: email } }).then(user => {
      if (user != undefined) {
        var correct = bcrypt.compareSync(password, user.password);
        
        if (correct) {
          const token = jwt.sign({ id: user._id }, configAuth.secret, { expiresIn: configAuth.expiresIn });
          return res.status(200).json({
            user: {
                id: user._id,
                name: user.name,
                email
            },
            token,
            error: false,
            message: 'Login realizado com sucesso!',
            //token: jwt.sign({ id: userExiste._id }, configAuth.secret, { expiresIn: configAuth.expiresIn }),
        })
          
         
        } else {
          return res.status(401).json({
            error: true,
            message: 'Login inválido!'
          })
        }
        

      }

      return res.status(401).json({
        error:true,
        message: 'Usuário não encontrado!'
      }); 

    })
  }
}

/*if (correct) {
  return res.json({
    user: {
      id: user._id,
      name: user.name,
      username: user.username,
      email
    },
    token: jwt.sign({ id: userExiste._id }, configAuth.secret, { expiresIn: configAuth.expiresIn }),
  })*/