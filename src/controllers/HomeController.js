const Home = require('../models/Home');

module.exports = {

  async index(req, res) {
    const home = await Home.findOne({
      where: {id:1}
    });

    return res.json(home);
  },

  async store(req, res) {

    try {
      const home = {
        title,
        subtitle,
        textBtn,
        linkBtn,
        serTitle,
        serSubtitle,
        serIconOne,
        serTitleOne,
        serDescOne,
        serIconTwo,
        serTitleTwo,
        serDescTwo,
        serIconThree,
        serTitleThree,
        serDescThree,
        portTitle,
        portSubtitle,
        portOriginalNameOne,
        portFileNameOne,
        portTitleOne,
        portSubtitleOne,
        portOriginalNameTwo,
        portFileNameTwo,
        portTitleTwo,
        portSubtitleTwo,
        portOriginalNameThree,
        portFileNameThree,
        portTitleThree,
        portSubtitleThree
      } = req.body;

      const rec = await Home.findOrCreate({
        where: {id: 1}
        
      });


      return res.json(rec);
    }catch(err) {
      res.json(err);
    }
  
  }
}
