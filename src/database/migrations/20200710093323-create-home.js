'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('homes', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      subtitle: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      textBtn: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      linkBtn: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serTitle: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serSubtitle: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serIconOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serTitleOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serDescOne: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      serIconTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serTitleTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serDescTwo: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      serIconThree: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serTitleThree: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      serDescThree: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      portTitle: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portSubtitle: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      portOriginalNameOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portFileNameOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portTitleOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portSubtitleOne: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      portOriginalNameTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portFileNameTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portTitleTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portSubtitleTwo: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      portOriginalNameThree: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portFileNameThree: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portTitleThree: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      portSubtitleThree: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('homes');
  }
}

