const { Model, DataTypes } = require('sequelize');

class Home extends Model {
  static init(sequelize) {
    super.init({
      title: DataTypes.STRING,
      subtitle: DataTypes.TEXT, 
      textBtn: DataTypes.STRING,
      linkBtn: DataTypes.STRING,
      serTitle: DataTypes.STRING,
      serSubtitle: DataTypes.TEXT,
      serIconOne: DataTypes.STRING,
      serTitleOne: DataTypes.STRING,
      serDescOne: DataTypes.TEXT,
      serIconTwo: DataTypes.STRING,
      serTitleTwo: DataTypes.STRING,
      serDescTwo: DataTypes.TEXT,
      serIconThree: DataTypes.STRING,
      serTitleThree: DataTypes.STRING,
      serDescThree: DataTypes.TEXT,
      portTitle: DataTypes.STRING,
      portSubtitle: DataTypes.TEXT,
      portOriginalNameOne: DataTypes.STRING,
      portFileNameOne: DataTypes.STRING,
      portTitleOne: DataTypes.STRING,
      portSubtitleOne: DataTypes.TEXT,
      portOriginalNameTwo: DataTypes.STRING,
      portFileNameTwo: DataTypes.STRING,
      portTitleTwo: DataTypes.STRING,
      portSubtitleTwo: DataTypes.TEXT,
      portOriginalNameThree: DataTypes.STRING,
      portFileNameThree: DataTypes.STRING,
      portTitleThree: DataTypes.STRING,
      portSubtitleThree: DataTypes.TEXT

    }, {
      sequelize,
      tableName: "homes",
      timestamps: true
    })
  }
  
}

module.exports = Home;

