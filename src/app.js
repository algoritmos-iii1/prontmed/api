const express = require('express');
const routes = require('./routes');
const cors = require('cors');
const path = require('path');

require('./database');

class App {
  constructor() {
    this.app = express();
    this.middlewares();
    this.routes();
  }
  middlewares() {
    this.app.use(express.json());
    this.app.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp', 'uploads', 'home'))
    );
    this.app.use((req, res, next) => {
      //console.log("Middleware!");
      //res.header("Access-Control-Allow-Origin", "http://localhost:3000");
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Methods", 'GET, POST, PUT, DELETE');
      this.app.use(cors());
      next();
    });
  }
  routes() {
    this.app.use(routes);
  }
}

module.exports = new App().app;
