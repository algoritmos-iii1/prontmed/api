const MedicalRecord = require('../models/MedicalRecord');
const Certificate = require('../models/Certificate');
const { index } = require('../controllers/MedicalRecordController');
const slugify = require('slugify');

module.exports = {

  async list(req, res) {
    const certificates = await Certificate.findAll();

    return res.json(certificates);
  },

  async index(req, res) {

    const { mr_id } = req.params;

    const medicalRecord = await MedicalRecord.findByPk(mr_id, {
      include: { association: 'certificates' }
    });
    return res.json(medicalRecord);
    // return res.json(medicalRecord.certificates);
  },

  async store(req, res) {

    try {

      const { mr_id } = req.params;
      const { title, body } = req.body;

      const medicalRecord = await MedicalRecord.findByPk(mr_id);

      if (!medicalRecord) {
        return res.status(400).json({
          error: true,
          message: 'Erro: Prontuário não encontrado!'
        });
      }

      const certificate = await Certificate.create({
        title,
        slug: slugify(title),
        body,
        mr_id
      });
      return res.status(200).json(certificate);
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  },

  async delete(req, res) {
    try {

      const { title } = req.body;
      const slug = slugify(title);

      const certificate = await Certificate.findOne({
        where: { slug }
      });

      if (!certificate) {
        return res.status(400).json({
          error: true,
          message: "Atestado não encontrado!"
        })
      }

      await Certificate.destroy({
        where: { slug }
      });

      return res.status(200).json({
        error: false,
        message: "Exclusão realizada com sucesso!"
      })
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }
}