const MedicalRecord = require('../models/MedicalRecord');
const ExamRequest = require('../models/ExamRequest');
const { index } = require('../controllers/MedicalRecordController');
const slugify = require('slugify');

module.exports = {

  async list(req, res) {
    const examRequests = await ExamRequest.findAll();

    return res.json(examRequests);
  },

  async index(req, res) {

    const { mr_id } = req.params;

    const medicalRecord = await MedicalRecord.findByPk(mr_id, {
      include: { association: 'exam_requests' }
    });
    return res.json(medicalRecord);
    // return res.json(medicalRecord.examRequests);
  },

  async store(req, res) {

    try {

      const { mr_id } = req.params;
      const { title, body } = req.body;

      const medicalRecord = await MedicalRecord.findByPk(mr_id);

      if (!medicalRecord) {
        return res.status(400).json({
          error: true,
          message: 'Erro: Prontuário não encontrado!'
        });
      }

      const examRequest = await ExamRequest.create({
        title,
        slug: slugify(title),
        body,
        mr_id
      });
      return res.status(200).json(examRequest);
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  },

  async delete(req, res) {
    try {

      const { title } = req.body;
      const slug = slugify(title);

      const examRequest = await ExamRequest.findOne({
        where: { slug }
      });

      if (!examRequest) {
        return res.status(400).json({
          error: true,
          message: "Exame não encontrado!"
        })
      }

      await ExamRequest.destroy({
        where: { slug }
      });

      return res.status(200).json({
        error: false,
        message: "Exclusão realizada com sucesso!"
      })
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }
}