const { Model, DataTypes, UUID } = require('sequelize');

class MedicalRecord extends Model {
    static init(sequelize) {
        super.init({
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            slug: {
                type: DataTypes.STRING,
                allowNull: false
            }
        }, {
            sequelize,
            tableName: "medical_records",
            timestamps: true,
            title: UUID,
            slug: UUID
        })
    }

    static associate(models) {
        this.hasMany(models.Exam, { foreignKey: 'mr_id', as: 'exams' });
        this.hasMany(models.ExamRequest, { foreignKey: 'mr_id', as: 'exam_requests' });
        this.hasMany(models.Document, { foreignKey: 'mr_id', as: 'documents' });
        this.hasMany(models.Record, { foreignKey: 'mr_id', as: 'records' });
        this.hasMany(models.Certificate, { foreignKey: 'mr_id', as: 'certificates' });
    }
}

module.exports = MedicalRecord;
