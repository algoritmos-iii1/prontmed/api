const { Model, DataTypes } = require('sequelize');
const { tableName } = require('./MedicalRecord');

class Exam extends Model {
    static init(sequelize) {
        super.init({
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            slug: {
                type: DataTypes.STRING,
                allowNull: false
            },
            body: {
                type: DataTypes.TEXT,
                allowNull: true
            },
        }, {
            sequelize,
            tableName: "exams",
            timestamps: true,
        })
    }

    static associate(models) {
        this.belongsTo(models.MedicalRecord, { foreignKey: 'mr_id', as: 'medicalRecord' });
    }

}

module.exports = Exam;

