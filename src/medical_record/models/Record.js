const { Model, DataTypes, UUID } = require('sequelize');
const { tableName } = require('./MedicalRecord');

class Record extends Model {
  static init(sequelize) {
    super.init({
      title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      slug: {
        type: DataTypes.STRING,
        allowNull: false
      },
      body: {
        type: DataTypes.TEXT,
        allowNull: true
      },
    }, {
      sequelize,
      tableName: "records",
      timestamps: true,
      title: UUID,
      slug: UUID
    })
  }

  static associate(models) {
    this.belongsTo(models.MedicalRecord, { foreignKey: 'mr_id', as: 'medicalRecord' });
  }

}

module.exports = Record;

